from rest_framework import routers
from rest_api.temperatures.views import TemperatureViewset

router = routers.DefaultRouter()
router.register(r'temperatures', TemperatureViewset)