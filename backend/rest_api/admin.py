from django.contrib import admin
from rest_api.models import (
    Temperature,
    User,
    Device,
)

admin.site.register(Temperature)
admin.site.register(User)
admin.site.register(Device)

