from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    accepted_terms_and_conditions_at = models.DateTimeField(null=True)
