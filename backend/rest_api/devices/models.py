from django.db import models
from django.conf import settings


class Device(models.Model):
    name = models.TextField()
    place = models.TextField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
