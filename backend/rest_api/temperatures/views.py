from rest_framework import viewsets
from rest_api.temperatures.models import Temperature
from rest_api.temperatures.serializers import TemperatureSerializer


class TemperatureViewset(viewsets.ModelViewSet):
    queryset = Temperature.objects.all()
    serializer_class = TemperatureSerializer
