from django.db import models


class Temperature(models.Model):
    temperature = models.FloatField()
    device = models.ForeignKey('Device', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
