from rest_framework import serializers
from rest_api.temperatures.models import Temperature


class TemperatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Temperature
        fields = ('temperature', 'device')
